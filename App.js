import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import Login from './src/login';
import Home from './src/home';
import FCR from './src/fcr';
import Feed from './src/feed';

const Stack = createNativeStackNavigator();
 const App =()=>{
    return(
      <NavigationContainer >
      <Stack.Navigator >
         <Stack.Screen name="Login" component={Login}  options={{ title: 'Poultry Calculator' }}/>
         <Stack.Screen name="Home" component={Home} options={{ title: 'Dashboard' }}/>
         <Stack.Screen name="FCR" component={FCR} options={{ title: 'FCR Result' }}/>
         <Stack.Screen name="Feed" component={Feed} options={{ title: 'Feed Usage' }}/>
      </Stack.Navigator>
    </NavigationContainer>
    )
 }
 export default App;