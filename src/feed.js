import React, {useState } from 'react'
import { Text, View,StyleSheet, ImageBackground, TouchableOpacity , TextInput } from 'react-native';

import  bkgimg from './asserts/bkg2.png' 


const Feed = ({ navigation }) => {

   const[res,setres] = useState(false)
   const[err,seterr] = useState('')
   const[input,setInput] = useState({
       tbirds:'',
       age:'',
       bag:'',
       day:''
   })
   const[out,setOut] = useState({
       use:'',
       pday:''
   })

   const pressbtn =()=>{
       const check = validate(input)
       if(check.valid===true){
           seterr('')
           setres(true)
           calc()
       }
       else{
           seterr('**'+check.error)
           setres(false)
       }
   }


   const calc=()=>{
      let t1 = parseFloat(input.tbirds/100)
      let ta = parseFloat(input.age/7)
      let tb = parseFloat(ta+1)
      let tc = parseFloat(tb*2)
      let td = parseFloat(tc*t1)
      let te = parseFloat(td/7)
      let tf = parseFloat(te*80)
      let tg = parseFloat(tf/input.bag)
      let th = parseFloat(tg*input.day)
      let tuse = parseFloat(th/10).toFixed(3)

      let ti = parseFloat(tg*1)
      let tday = parseFloat(ti/10).toFixed(2)


       setOut({...out, use:tuse, pday:tday})
   }


  return (
     <View > 
     <ImageBackground  style={styles.img} source={bkgimg}>
        <View style={styles.container}>
           <View style={styles.inputView} >
               <TextInput  
                   style={styles.inputText}
                   placeholder="Total Birds..." 
                   placeholderTextColor="#003f5c"
                   onChangeText={text => setInput({...input,tbirds:text})}/>
           </View>
           <View style={styles.inputView} >
               <TextInput  
                   style={styles.inputText}
                   placeholder="Birds Age..." 
                   placeholderTextColor="#003f5c"
                   onChangeText={text => setInput({...input,age:text})}/>
           </View>
           <View style={styles.inputView} >
               <TextInput  
                   style={styles.inputText}
                   placeholder="Feed Bag Weight Kg..." 
                   placeholderTextColor="#003f5c"
                   onChangeText={text => setInput({...input,bag:text})}/>
           </View>
           <View style={styles.inputView} >
               <TextInput  
                   style={styles.inputText}
                   placeholder="Days..." 
                   placeholderTextColor="#003f5c"
                   onChangeText={text => setInput({...input,day:text})}/>
           </View>
           <View>
               <Text style={{color:'purple',fontSize:14}}>{err}</Text>
           </View>
           <TouchableOpacity style={styles.loginBtn} onPress={pressbtn}>
               <Text style={styles.loginText}>Result</Text>
           </TouchableOpacity>
           {!res?<View style={styles.out}>
               <Text style={styles.outText}>Feed Usage:-</Text>
           </View>:
           <View style={styles.out} >
              <Text style={styles.outText}>Feed Usage:-</Text>
               <Text style={styles.outText}>Feed Use: {out.use}</Text>
               <Text style={styles.outText}>Per Day Use: {out.pday}</Text>
           </View>}
           
        </View>
     </ImageBackground>      
   </View>
  )
}

const styles = StyleSheet.create({
  container: {
     flex: 1,
     alignItems: 'center',
     justifyContent: 'center',
   },
   img:{
       width:'100%',
       height:'100%',
    },
    inputView:{
       width:"90%",
       backgroundColor:'lightgray',
       borderRadius:5,
       height:40,
       marginBottom:10,
       justifyContent:"center",
       padding:10,
     },
     inputText:{
       fontSize:15,
       height:40,
       color:"black",
       fontWeight:'bold'
     },
     loginBtn:{
       width:"40%",
       backgroundColor:"#fb5b5a",
       borderRadius:25,
       height:40,
       alignItems:"center",
       justifyContent:"center",
       marginBottom:10
     },
     loginText:{
       color:"white",
       fontSize:18
     },
     outText:{
       color:"black",
       flexDirection:'column',
       fontSize:18,
       padding:5,
       fontWeight:'bold'
     },
     out:{
         width:'80%'
     }
})

const validate =(value)=>{
if(value===null){
   return ({error:'Fields Empty',valid:false})
}
if(value.tbirds===''){
   return ({error:'Enter Total Birds',valid:false})
}
if(value.age===''){
   return ({error:'Enter Birds Age',valid:false})
}
if(value.bag===''){
   return ({error:'Enter Feed Bag weight',valid:false})
}
if(value.day===''){
   return ({error:'Enter days',valid:false})
}
return ({error:'Looks Fine',valid:true})
}


export default Feed;