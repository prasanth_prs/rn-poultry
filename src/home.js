import React from 'react'
import { Text, View,StyleSheet, ImageBackground, TouchableOpacity , Image } from 'react-native';

import  bkgimg from './asserts/bkg_home.png' 
import fcr from './asserts/final_res.png'
import feed from './asserts/hen_ico.png'

const Home = ({ navigation }) => {

   return (
      <View > 
      <ImageBackground  style={styles.img} source={bkgimg}>
      <View flexDirection='row' style={{justifyContent:'space-between'}}>
         <View style={styles.container}>
            <TouchableOpacity style={styles.btn}
               onPress={() =>
               navigation.navigate('FCR')
               }>
                  <Text style={styles.btntxt}> FCR</Text>
                  <Image style={styles.imgIco} source={fcr}></Image>
            </TouchableOpacity>
         </View>
         <View style={styles.container}>
            <TouchableOpacity style={styles.btn}
               onPress={() =>
               navigation.navigate('Feed')
               }>
                  <Text style={styles.btntxt}> Feed Usage</Text>
                  <Image  style={styles.imgIco} source={feed}></Image>
            </TouchableOpacity>
         </View>
         </View>
         <Text style={{fontSize:16,color:'#462686', padding:5, textAlign:'center'}}>Copyright:-prsnthmailbox@gmail.com</Text>
      </ImageBackground>      
    </View>
   )
}

const styles = StyleSheet.create({
   container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
    },
    btn:{
       width:'90%',
       height:'50%',
       //backgroundColor:'transparent',
       backgroundColor:'#CCFFFF',
       borderRadius:10,
       fontWeight:'bold',
       color:'white',
      justifyContent: 'center',
    },
    btntxt:{
      fontSize:20,
      fontWeight:'bold',
      color:'#004466',
   },
    img:{
       width:'100%',
       height:'100%',
    },
    imgIco:{
      flex: 1,
      width: 100,
      height: 100,
      resizeMode: 'contain',
      alignItems:'center',
      justifyContent:'center'
   }
})

export default Home;