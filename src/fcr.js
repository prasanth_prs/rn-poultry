import React, {useState } from 'react'
import { TextInput, View,StyleSheet, ImageBackground, TouchableOpacity, Text , ScrollView } from 'react-native';

import  bkgimg from './asserts/bkg2.png' 

const FCR = ({ navigation }) => {

    const[res,setres] = useState(false)
    const[err,seterr] = useState('')
    const[input,setInput] = useState({
        tbirds:'',
        sbirds:'',
        age:'',
        tweight:'',
        tfeed:''
    })
    const[out,setOut] = useState({
        avg:'',
        mor:'',
        morp:'',
        fcr:'',
        cfcr:'',
        eef:'',
        fin:'',
        day:''
    })

    const pressbtn =()=>{
        const check = validate(input)
        if(check.valid===true){
            seterr('')
            setres(true)
            calc()
        }
        else{
            seterr('**'+check.error)
            setres(false)
        }
    }

 
    const calc=()=>{
        let bal = input.tbirds - input.sbirds
        let awt = (input.tweight / input.sbirds).toFixed(2)
        let fi = (input.tfeed / input.sbirds).toFixed(2)
        let fc = (input.tfeed / input.tweight).toFixed(2)
        let cf1 = 2-awt; let cf2 = cf1*0.3; let cf3 = parseFloat(cf2) + parseFloat(fc)
        let cfc = cf3.toFixed(2)
        let dg = ((awt*1000)/(input.age)).toFixed(2)
        let mp = ((bal / input.tbirds)*100).toFixed(2)
        let val1 = 100 - mp
        let val2 = (fc * 10)
        let ef = ((dg * val1)/ val2).toFixed(2)

        setOut({...out, avg:awt, mor:bal, morp:mp, fcr:fc, cfcr:cfc, eef:ef, fin:fi, day:dg})
    }


   return (
      <View > 
      <ImageBackground  style={styles.img} source={bkgimg}>
         <View style={styles.container}>
            <View style={styles.inputView} >
                <TextInput  
                    style={styles.inputText}
                    placeholder="Total Birds..." 
                    placeholderTextColor="#003f5c"
                    onChangeText={text => setInput({...input,tbirds:text})}/>
            </View>
            <View style={styles.inputView} >
                <TextInput  
                    style={styles.inputText}
                    placeholder="Sold Birds..." 
                    placeholderTextColor="#003f5c"
                    onChangeText={text => setInput({...input,sbirds:text})}/>
            </View>
            <View style={styles.inputView} >
                <TextInput  
                    style={styles.inputText}
                    placeholder="Age..." 
                    placeholderTextColor="#003f5c"
                    onChangeText={text => setInput({...input,age:text})}/>
            </View>
            <View style={styles.inputView} >
                <TextInput  
                    style={styles.inputText}
                    placeholder="Total Weight in Kg..." 
                    placeholderTextColor="#003f5c"
                    onChangeText={text => setInput({...input,tweight:text})}/>
            </View>
            <View style={styles.inputView} >
                <TextInput  
                    style={styles.inputText}
                    placeholder="Feed Used in Kg..." 
                    placeholderTextColor="#003f5c"
                    onChangeText={text => setInput({...input,tfeed:text})}/>
            </View>
            <View>
                <Text style={{color:'purple',fontSize:14}}>{err}</Text>
            </View>
            <TouchableOpacity style={styles.loginBtn} onPress={pressbtn}>
                <Text style={styles.loginText}>Result</Text>
            </TouchableOpacity>
            {!res?<View style={styles.out}>
                <Text style={styles.outText}>Farm Performance:-</Text>
            </View>:
            <View style={styles.out} >
                <View flexDirection='row' style={{justifyContent:'space-between'}}>
                    <Text style={styles.outText}>Avg Wt: {out.avg}</Text>
                    <Text style={styles.outText}>EEF: {out.eef}</Text>
                </View>
                <View flexDirection='row' style={{justifyContent:'space-between'}}>
                    <Text style={styles.outText}>FCR: {out.fcr}</Text>
                    <Text style={styles.outText}>CFCR: {out.cfcr}</Text>
                </View>
                <View flexDirection='row' style={{justifyContent:'space-between'}}>
                    <Text style={styles.outText}>Mortality: {out.mor}</Text>
                    <Text style={styles.outText}>Mor%: {out.morp}</Text>
                </View>
                <View flexDirection='row' style={{justifyContent:'space-between'}}>
                    <Text style={styles.outText}>Feed-In: {out.fin}</Text>
                    <Text style={styles.outText}>Day Gain: {out.day}</Text>
                </View>
            </View>}
            
         </View>
      </ImageBackground>      
    </View>
   )
}

const styles = StyleSheet.create({
   container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
    },
    img:{
        width:'100%',
        height:'100%',
     },
     inputView:{
        width:"90%",
        backgroundColor:'lightgray',
        borderRadius:5,
        height:40,
        marginBottom:10,
        justifyContent:"center",
        padding:10,
      },
      inputText:{
        fontSize:15,
        height:40,
        color:"black",
        fontWeight:'bold'
      },
      loginBtn:{
        width:"40%",
        backgroundColor:"#fb5b5a",
        borderRadius:25,
        height:40,
        alignItems:"center",
        justifyContent:"center",
        marginBottom:10
      },
      loginText:{
        color:"white",
        fontSize:18
      },
      outText:{
        color:"black",
        flexDirection:'column',
        fontSize:18,
        padding:5,
        fontWeight:'bold'
      },
      out:{
          width:'80%'
      }
})

const validate =(value)=>{
if(value===null){
    return ({error:'Fields Empty',valid:false})
}
if(value.tbirds===''){
    return ({error:'Enter Total Birds',valid:false})
}
if(value.sbirds===''){
    return ({error:'Enter Total Sold Birds',valid:false})
}
if(value.age===''){
    return ({error:'Enter Birds Age',valid:false})
}
if(value.tweight===''){
    return ({error:'Enter Total Birds Weight',valid:false})
}
if(value.tfeed===''){
    return ({error:'Enter Total Feed used',valid:false})
}
return ({error:'Looks Fine',valid:true})
}

export default FCR;