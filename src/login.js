import React, { useState } from 'react';
import { StyleSheet, Text, View, TextInput, TouchableOpacity,Alert, Image } from 'react-native';
import logo from './asserts/mylogo.png'

export default function Login({ navigation }) {

  const [email, setemail] = useState('')
  const [pwd, setpwd] = useState('')

const lgnPress=()=>{
  // if(email==='' || pwd===''){
  //   Alert.alert("Enter Valid Credits")
  // }else if(email === 'prs' && pwd === 'prs'){
  //   navigation.navigate('Home')
  // }
  navigation.navigate('Home')
}

  return (
    <View style={styles.container}>
    <Image style={styles.logo} source={logo}></Image>
    <View style={styles.inputView} >
      <TextInput  
        style={styles.inputText}
        placeholder="Username..." 
        onChangeText={text => setemail(text)}/>
    </View>
    <View style={styles.inputView} >
      <TextInput  
        secureTextEntry
        style={styles.inputText}
        placeholder="Password..." 
        onChangeText={text => setpwd(text)}/>
    </View>
    <TouchableOpacity>
      <Text></Text>
    </TouchableOpacity>
    <TouchableOpacity style={styles.loginBtn} onPress={lgnPress}>
      <Text style={styles.loginText}>LOGIN</Text>
    </TouchableOpacity>
  </View>
  );
};


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#003f5c',
    alignItems: 'center',
    justifyContent: 'center',
  },
  logo:{
    marginBottom:20
  },
  inputView:{
    width:"90%",
    backgroundColor:"#465881",
    borderRadius:25,
    height:50,
    marginBottom:18,
    justifyContent:"center",
    padding:20
  },
  inputText:{
    fontSize:20,
    height:50,
    color:"white"
  },
  loginBtn:{
    width:"50%",
    backgroundColor:"#fb5b5a",
    borderRadius:25,
    height:50,
    alignItems:"center",
    justifyContent:"center",
    marginTop:10,
    marginBottom:10
  },
  loginText:{
    color:"white",
    fontSize:18
  }
});
