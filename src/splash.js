import React from 'react';
import {StyleSheet,View,Text,ActivityIndicator} from 'react-native';


const SplashScreen = ({navigation}) => {
  const [animating, setAnimating] = useState(true);

  useEffect(() => {
    setTimeout(() => {
      setAnimating(false);
    }, 5000);
    navigation.navigate('Login', { name: 'Jane' })
  }, []);

  return (
    <View style={styles.splshcont}>
      <Text style={styles.splshttitle}>PRS DEVELOP</Text>
      <ActivityIndicator
        animating={animating}
        color="#FFFFFF"
        size="large"
        style={styles.activityIndicator}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  splshcont:{
    flex:1,
    alignItems:'center',
    justifyContent:'center'
  },
  splshttitle:{
    fontWeight:'bold',
    fontSize:20,
    color:'black'
  },
  activityIndicator: {
    alignItems: 'center',
    height: 80,
  }
})

export default SplashScreen;